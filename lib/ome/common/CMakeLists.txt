# #%L
# OME C++ libraries (cmake build infrastructure)
# %%
# Copyright © 2006 - 2015 Open Microscopy Environment:
#   - Massachusetts Institute of Technology
#   - National Institutes of Health
#   - University of Dundee
#   - Board of Regents of the University of Wisconsin-Madison
#   - Glencoe Software, Inc.
# Copyright © 2018 - 2019 Quantitative Imaging Systems, LLC
# %%
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# The views and conclusions contained in the software and documentation are
# those of the authors and should not be interpreted as representing official
# policies, either expressed or implied, of any organization.
# #L%

configure_file(${CMAKE_CURRENT_SOURCE_DIR}/config.h.in
               ${CMAKE_CURRENT_BINARY_DIR}/config.h @ONLY)
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/config-internal.h.in
               ${CMAKE_CURRENT_BINARY_DIR}/config-internal.h @ONLY)

set(ome_common_static_headers
    module.h)

set(ome_common_generated_private_headers
   ${CMAKE_CURRENT_BINARY_DIR}/config-internal.h)

set(ome_common_generated_headers
    ${CMAKE_CURRENT_BINARY_DIR}/config.h)

set(ome_common_headers
    ${ome_common_static_headers}
    ${ome_common_generated_headers}
    ${ome_common_generated_private_headers})

set(ome_common_sources
    module.cpp)

add_library(ome-common ${ome_common_sources} ${ome_common_headers})

target_include_directories(ome-common PUBLIC
                           $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/lib>
                           $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/lib>
                           $<BUILD_INTERFACE:${LibDl_INCLUDE_DIRS}>
                           $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>)

target_link_libraries(ome-common
                      PUBLIC
                      Threads::Threads
                      ${EXTRA_FILESYSTEM_LIBS}
                      ${CMAKE_DL_LIBS}
                      fmt::fmt
                      PRIVATE
                      ${LibDl_LIBRARIES})

set(OME_COMMON_FIND_DEPENDENCIES
    Threads)

set_target_properties(ome-common PROPERTIES LINKER_LANGUAGE CXX)
set_target_properties(ome-common PROPERTIES VERSION ${ome-common_VERSION})
set_target_properties(ome-common PROPERTIES EXPORT_NAME "OME::Common")

add_library(OME::Common ALIAS ome-common)

if(WIN32)
  set(ome_common_config_dir "cmake")
else()
  set(ome_common_config_dir "${CMAKE_INSTALL_LIBDIR}/cmake/OMECommon")
endif()

install(TARGETS ome-common
        EXPORT OMECommonInternal
        RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
        LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
        ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
        COMPONENT "runtime"
        INCLUDES DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}")
install(EXPORT OMECommonInternal
        DESTINATION "${ome_common_config_dir}"
        COMPONENT "development")

include(CMakePackageConfigHelpers)
configure_package_config_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/OMECommonConfig.cmake.in"
  "${CMAKE_CURRENT_BINARY_DIR}/OMECommonConfig.cmake"
  INSTALL_DESTINATION "${ome_common_config_dir}")
write_basic_package_version_file(
  ${CMAKE_CURRENT_BINARY_DIR}/OMECommonConfigVersion.cmake
  VERSION "${ome-common_VERSION}"
  COMPATIBILITY SameMajorVersion)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/OMECommonConfig.cmake
              ${CMAKE_CURRENT_BINARY_DIR}/OMECommonConfigVersion.cmake
        DESTINATION "${ome_common_config_dir}"
        COMPONENT "development")

set(ome_common_includedir "${CMAKE_INSTALL_INCLUDEDIR}/ome/common")

install(FILES ${ome_common_static_headers} ${ome_common_generated_headers}
        DESTINATION ${ome_common_includedir}
        COMPONENT "development")

# Dump header list for testing
header_include_list_write(ome_common_static_headers ome_common_generated_headers ome/common ${PROJECT_BINARY_DIR}/test/ome-common)
